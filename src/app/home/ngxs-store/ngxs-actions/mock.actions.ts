export class SaveTradingModeAction {
    static readonly type = "[MOCK] Save trading mode";
    constructor(public payload: any) { }
}
export class SaveTradingModeIconStatusAction {
    static readonly type = "[MOCK] Save trading mode icon status";
    constructor(public payload: any) { }
}
export class SaveStockTradingDetailsAction {
    static readonly type = "[MOCK] Save stock trading details";
    constructor(public payload: any) { }
}
export class ResetSpecificMockStateAction {
    static readonly type = "[MOCK] Reset specific mock state";
    constructor(public payload: any) { }
}