import { Navigate } from "@ngxs/router-plugin";
import {
  ForgotPasswordAction,
  LoginAction,
  SetPasswordAction,
  ChangePasswordAction,
} from "./../ngxs-actions/login.actions";
import { GetUserInfoAction } from "./../ngxs-actions/user.actions";
import { Injectable } from "@angular/core";
import { State, Action, StateContext, Selector, Store } from "@ngxs/store";
import { ApiService } from "@app/home/services/api.service";
import { HideLoaderAction } from "../ngxs-actions/loader.actions";
import {
  ShowToastAction,
  ErrorApiToastAction,
} from "../ngxs-actions/toast.actions";
import * as _ from "lodash";
import { Router } from "@angular/router";
import { AuthService } from "auth";
import { UserState } from "./UserState";

export interface LoginStateModel { }

@State({
  name: "loginState",
  defaults: {},
})
@Injectable()
export class LoginState {
  constructor(
    private store: Store,
    private apiService: ApiService,
    private authService: AuthService,
    private router: Router
  ) { }

  @Action(ForgotPasswordAction)
  forgotPasswordAction(
    ctx: StateContext<LoginStateModel>,
    action: ForgotPasswordAction
  ) {
    this.apiService
      .get(`main/auth/forgot-password/${action.email}`, {}, false)
      .subscribe(
        (response: any) => {
          this.store.dispatch(new HideLoaderAction());
          if (_.get(response, "status", 500) === 200) {
            this.store.dispatch(
              new ShowToastAction(response.message, "success")
            );
            this.store.dispatch(new Navigate(["signin"]));
          } else {
            this.store.dispatch(
              new ShowToastAction("Something Went Wrong!!", "warning")
            );
          }
        },
        (error) => {
          this.store.dispatch(new ErrorApiToastAction(error));
        }
      );
  }

  @Action(LoginAction)
  loginAction(ctx: StateContext<LoginStateModel>, action: LoginAction) {
    this.apiService.login(action).subscribe(
      ({ data, message, status }: any) => {
        if (status === 200) {
          this.authService.setAccessToken(data.token);
          if (data.is_verified) {
            this.store.dispatch(new GetUserInfoAction(data));
            localStorage.setItem("userData", JSON.stringify(data));
            this.store.dispatch(new Navigate(["myaccount"]));
          } else {
            this.store.dispatch(new ShowToastAction("Not Verified"));
            this.store.dispatch(new Navigate(["signin"]));
          }
        } else {
          this.store.dispatch(new ShowToastAction(message));
        }
        this.store.dispatch(new HideLoaderAction());
      },
      (error) => {
        this.store.dispatch(new ErrorApiToastAction(error));
      }
    );
  }

  @Action(SetPasswordAction)
  setPasswordAction(
    ctx: StateContext<LoginStateModel>,
    action: SetPasswordAction
  ) {
    this.apiService
      .post(`main/auth/verify/${action.token}`, {
        newpassword: action.password,
      })
      .subscribe(
        (response: any) => {
          this.store.dispatch(new ShowToastAction(response.message, "success"));
          this.store.dispatch(new HideLoaderAction());
          this.store.dispatch(new Navigate(["signin"]));
        },
        (error) => {
          this.store.dispatch(new ErrorApiToastAction(error));
        }
      );
  }

  @Action(ChangePasswordAction)
  changePassword(
    ctx: StateContext<LoginStateModel>,
    action: ChangePasswordAction
  ) {
    this.apiService
      .post(
        `main/users/change-password`,
        {
          id: this.store.selectSnapshot(UserState.userId),
          new_password: action.newPassword,
          old_password: action.currentPassword,
        },
        false
      )
      .subscribe(
        (response: any) => {
          this.store.dispatch(new ShowToastAction(response.message, "success"));
          this.store.dispatch(new HideLoaderAction());
          this.store.dispatch(new Navigate(["settings"]));
        },
        (error) => {
          this.store.dispatch(new ErrorApiToastAction(error));
        }
      );
  }
}
