import { LoginState } from "./LoginState";
import { LoaderState } from "./LoaderState";
import { ToastState } from "./ToastState";
import { UserState } from "./UserState";
import { MockState } from "./MockState";

export const rootStates = [
  LoaderState,
  ToastState,
  LoginState,
  UserState,
  MockState
];
