import { Injectable } from "@angular/core";
import { State, Action, StateContext, Selector, Store } from "@ngxs/store";
import {
    SaveTradingModeAction,
    ResetSpecificMockStateAction,
    SaveStockTradingDetailsAction,
    SaveTradingModeIconStatusAction
} from '../ngxs-actions/mock.actions';

export interface MockStateModel {
    showTradingModeIcon: boolean
    tradingMode: any;
    stockTradingDetails: any;
}

@State({
    name: "mockState",
    defaults: {
        showTradingModeIcon: false,
        tradingMode: {},
        stockTradingDetails: {},
    },
})
@Injectable()
export class MockState {
    constructor(private store: Store) { }

    @Action(SaveStockTradingDetailsAction)
    saveStockTradingDetails(
        ctx: StateContext<MockStateModel>,
        action: SaveStockTradingDetailsAction
    ) {
        ctx.patchState({ stockTradingDetails: action.payload });
    }

    @Action(SaveTradingModeAction)
    saveTradingMode(
        ctx: StateContext<MockStateModel>,
        action: SaveTradingModeAction
    ) {
        ctx.patchState({ tradingMode: action.payload });
    }
    @Action(SaveTradingModeIconStatusAction)
    saveTradingModeIconStatus(
        ctx: StateContext<MockStateModel>,
        action: SaveTradingModeIconStatusAction
    ) {
        ctx.patchState({ showTradingModeIcon: action.payload });
    }

    @Action(ResetSpecificMockStateAction)
    resetSpecificMockState(
        ctx: StateContext<MockStateModel>,
        action: ResetSpecificMockStateAction
    ) {
        ctx.patchState({ ...action.payload });
    }

}