import { rootStates } from "./ngxs-store/ngxs-state/index";
import { NgModule, Optional, SkipSelf } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import {
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule } from "@angular/forms";
import { DynamicFormsMaterialUIModule } from "@ng-dynamic-forms/ui-material";
import { MatTooltipModule } from "@angular/material";
import { environment } from "@env/environment";
import { FormsModule } from "@angular/forms";
import { UtilsModule, LoggerService } from "utils";
import { AngularMaterialModule } from "utils";
import { DynamicFormsModule } from "dynamic-forms";
import { throwIfAlreadyLoaded } from "./module-import-guard";
import { ChartsModule } from "ng2-charts";
import { ToastrModule } from "ngx-toastr";
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxsModule } from "@ngxs/store";
import { NgxsReduxDevtoolsPluginModule } from "@ngxs/devtools-plugin";
import { NgxsLoggerPluginModule } from "@ngxs/logger-plugin";
import { NgxsResetPluginModule } from "ngxs-reset-plugin";
import { NgxsRouterPluginModule } from "@ngxs/router-plugin";
import { NgOtpInputModule } from 'ng-otp-input';
import { CarouselModule } from 'ngx-owl-carousel-o';


import { CustomToast } from "./components/custom-toast/customtoast.component";
import { NavigationBarComponent } from "./components/navigation-bar/navigation-bar.component";
import { NavComponent } from "./components/nav/nav.component";
import { SignInComponent } from './components/login/sign-in/sign-in.component';
import { ForgotPasswordComponent } from './components/login/forgot-password/forgot-password.component';
import { ReviewComponent } from './components/review/review.component';
import { ProfileComponent } from './components/profile/profile.component';
import { VerifyNumberComponent } from './components/login/verify-number/verify-number.component';
import { CreateAccountComponent } from './components/account/create-account/create-account.component';
import { IntroAccountComponent } from './components/account/intro-account/intro-account.component';
import { PanDetailsComponent } from './components/account/kyc/pan-details/pan-details.component';
import { AadhaarDetailsComponent } from './components/account/kyc/aadhaar-details/aadhaar-details.component';
import { BankDetailsComponent } from './components/account/kyc/bank-details/bank-details.component';
import { AdditionalDetailsComponent } from './components/account/kyc/additional-details/additional-details.component';
import { SignUpComponent } from './components/login/sign-up/sign-up.component';
import { HomeComponent } from './components/home/home.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { SignalsComponent } from './components/signals/signals.component';
import { TradingModeCardComponent } from './components/cards/trading-mode-card/trading-mode-card.component';
import { BalanceCardComponent } from './components/cards/balance-card/balance-card.component';
import { StocksCardComponent } from './components/cards/stocks-card/stocks-card.component';
import { InvestmentCardComponent } from './components/cards/investment-card/investment-card.component';
import { TradingModeComponent } from './components/trading-mode/trading-mode.component';
import { TradeStocksComponent } from './components/stocks/trade-stocks/trade-stocks.component';
import { TradeConfirmationComponent } from './components/stocks/trade-confirmation/trade-confirmation.component';
import { OnboardSlidesComponent } from './components/onboard-slides/onboard-slides.component';

@NgModule({
  imports: [
    // AngularFireModule.initializeApp(environment.firebase),
    // The Angular Material module must be imported after Angular's BrowserModule, as the import order matters for NgModules.
    AngularMaterialModule,
    BrowserAnimationsModule,
    CommonModule,
    FlexLayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    DynamicFormsModule,
    DynamicFormsMaterialUIModule,
    ChartsModule,
    MatTooltipModule,
    NgxSpinnerModule,
    NgOtpInputModule,
    CarouselModule,
    FormsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      // progressBar: true,
      progressBar: false,
      closeButton: true,
      // positionClass: "toast-top-full-width",
      positionClass: "toast-bottom-center",
      preventDuplicates: true,
      // toastComponent: CustomToast,
      // toastClass: "custom-toast",
      // messageClass: "custom-toast-message",
      // titleClass: "custom-toast-title"
    }),
    NgxsModule.forRoot(rootStates, {
      developmentMode: !environment.production,
    }),
    NgxsRouterPluginModule.forRoot(),
    NgxsResetPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot(),

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),

    UtilsModule.forRoot(environment),

    RouterModule, // There is no directive with "exportAs" set to "routerLinkActive ...
  ],
  entryComponents: [CustomToast, TradingModeComponent, TradeConfirmationComponent, IntroAccountComponent],
  declarations: [
    NavigationBarComponent,
    NavComponent,
    CustomToast,
    SignInComponent,
    ForgotPasswordComponent,
    ReviewComponent,
    ProfileComponent,
    VerifyNumberComponent,
    CreateAccountComponent,
    IntroAccountComponent,
    PanDetailsComponent,
    AadhaarDetailsComponent,
    BankDetailsComponent,
    AdditionalDetailsComponent,
    SignUpComponent,
    HomeComponent,
    PortfolioComponent,
    SignalsComponent,
    TradingModeCardComponent,
    BalanceCardComponent,
    StocksCardComponent,
    InvestmentCardComponent,
    TradingModeComponent,
    TradeStocksComponent,
    TradeConfirmationComponent,
    OnboardSlidesComponent,
  ],
  exports: [
    NavigationBarComponent,
    NavComponent,
    CustomToast,
    SignInComponent,
    ForgotPasswordComponent,
    ReviewComponent,
    ProfileComponent,
    VerifyNumberComponent,
    CreateAccountComponent,
    IntroAccountComponent,
    PanDetailsComponent,
    AadhaarDetailsComponent,
    BankDetailsComponent,
    AdditionalDetailsComponent,
    SignUpComponent,
    HomeComponent,
    PortfolioComponent,
    SignalsComponent,
    TradingModeCardComponent,
    BalanceCardComponent,
    StocksCardComponent,
    InvestmentCardComponent,
    TradingModeComponent,
    TradeStocksComponent,
    TradeConfirmationComponent
  ],
})
export class HomeModule {
  constructor(
    @Optional() @SkipSelf() parentModule: HomeModule,
    private translate: TranslateService,
    private logger: LoggerService
  ) {
    this.logger.info("Home Module initialised");

    // 'en-gb' -> 'en'
    const defaultLanguage = environment.defaultLanguage.split("-")[0];

    this.logger.info("Default Language: " + defaultLanguage);
    this.logger.info("Local: " + environment.defaultLanguage.split("-")[1]);

    translate.setDefaultLang(defaultLanguage);
    translate.use(defaultLanguage);

    throwIfAlreadyLoaded(parentModule, "HomeModule");
  }
}

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

