import { Component, EventEmitter, Output } from "@angular/core";
import { NavigationEnd, Router, ActivatedRoute } from "@angular/router";

import { AuthService } from "auth";

import { LoggerService } from "utils";
import { Location } from "@angular/common";
import * as _ from "lodash";
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";

@Component({
  selector: "app-navigation-bar",
  templateUrl: "./navigation-bar.component.html",
  styleUrls: ["./navigation-bar.component.scss"],
})
export class NavigationBarComponent {
  @Output() toggleSidenav = new EventEmitter<void>();
  tradingMode: string = null;
  private returnUrl = "/signin";
  public userInfo: any = "";
  public showAnalyserButton: boolean = false;
  public showBackButton: boolean = false;
  public showHeaderNavBar: boolean = true;
  public backButtonPath: string = "";
  public showTradingModeIcon: boolean = false

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private logger: LoggerService,
    private _location: Location,
    private store: Store
  ) {
    // this.router.events.subscribe((event) => {

    //   if (event instanceof NavigationEnd) {

    //     this.returnUrl = event.url;

    //     this.logger.info('NavigationBarComponent returnUrl: ' + this.returnUrl);
    //   }

    // });



    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.returnUrl = event.url;
        this.logger.info("NavigationBarComponent returnUrl: " + this.returnUrl);
        this.backButtonPath =
          route.root.firstChild.snapshot.data["backButtonPath"];
        // this.showAnalyserButton = this.returnUrl === "/spends";
        this.showBackButton =
          route.root.firstChild.snapshot.data["showBackButton"] || false;
        this.showHeaderNavBar =
          route.root.firstChild.snapshot.data["showHeaderNavBar"] === undefined
            ? true
            : route.root.firstChild.snapshot.data["showHeaderNavBar"];
      }
    });
  }

  backClicked() {
    if (_.isEmpty(this.backButtonPath)) {
      this._location.back();
    } else {
      this.store.dispatch(new Navigate([this.backButtonPath]));
    }
  }

  public logout() {
    this.authService.logout(this.returnUrl || "/");
  }
}
