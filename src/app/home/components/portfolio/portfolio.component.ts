import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  inputForm: FormGroup;

  constructor() { }

  ngOnInit() {
    this.inputForm = new FormGroup({
      mobileNo: new FormControl("", [Validators.required,
      Validators.pattern(/^[0-9]\d*$/),
      Validators.minLength(10),
      Validators.maxLength(10)]),
    });
  }
  submit() {}
    
  

}
