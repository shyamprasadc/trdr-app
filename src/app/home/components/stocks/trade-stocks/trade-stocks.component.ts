import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { mockData } from "@app/home/services/mock.service"
import { ActivatedRoute } from "@angular/router";
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";
import * as _ from "lodash";
import {
  SaveStockTradingDetailsAction
} from "@app/home/ngxs-store/ngxs-actions/mock.actions";
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TradeConfirmationComponent } from '@app/home/components/stocks/trade-confirmation/trade-confirmation.component';

@Component({
  selector: 'app-trade-stocks',
  templateUrl: './trade-stocks.component.html',
  styleUrls: ['./trade-stocks.component.scss']
})
export class TradeStocksComponent implements OnInit {
  inputForm: FormGroup;
  company: string = null;
  stockDetails: any;
  totalPrice: number = 0

  constructor(private store: Store, private activatedRoute: ActivatedRoute, private dialog: MatDialog) {
    this.company = this.activatedRoute.snapshot.paramMap.get(
      "company"
    );
    if (!this.company) {
      this.cancelClick()
    }
  }

  ngOnInit() {
    const allStocks = _.get(mockData, "stocks")
    if (!_.isEmpty(this.company)) {
      this.stockDetails = _.filter(allStocks, { company: this.company })[0]
    }

    this.inputForm = new FormGroup({
      quantity: new FormControl("1", [Validators.required, Validators.pattern(/^[0-9]\d*$/)]),
    });
  }

  calculateTotal() {
    this.totalPrice = this.stockDetails.price * this.inputForm.controls.quantity.value;
    return this.totalPrice
  }

  buyClick() {
    const payload = {
      stockDetails: this.stockDetails,
      quantity: this.inputForm.controls.quantity.value,
      totalPrice: this.totalPrice,
      type: "BUY"
    }
    this.store.dispatch(new SaveStockTradingDetailsAction(payload));
    this.popUpConfirmation()
  }

  sellClick() {
    const payload = {
      stockDetails: this.stockDetails,
      quantity: this.inputForm.controls.quantity.value,
      totalPrice: this.totalPrice,
      type: "SELL"
    }
    this.store.dispatch(new SaveStockTradingDetailsAction(payload));
    this.popUpConfirmation()
  }

  cancelClick() {
    this.store.dispatch(new Navigate(["signals"]));
  }

  popUpConfirmation() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "80%";
    dialogConfig.maxWidth = "352px"

    this.dialog.open(TradeConfirmationComponent, dialogConfig)
  }

}
