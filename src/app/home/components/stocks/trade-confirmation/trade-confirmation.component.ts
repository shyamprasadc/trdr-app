import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from "@ngxs/store";
import * as _ from "lodash";
import { Navigate } from "@ngxs/router-plugin";
import { MatDialogRef } from "@angular/material";
import { ResetSpecificMockStateAction } from '@app/home/ngxs-store/ngxs-actions/mock.actions';


@Component({
  selector: 'app-trade-confirmation',
  templateUrl: './trade-confirmation.component.html',
  styleUrls: ['./trade-confirmation.component.scss']
})
export class TradeConfirmationComponent implements OnInit, OnDestroy {
  stockTradeDetails: any;
  constructor(private store: Store, public dialogRef: MatDialogRef<TradeConfirmationComponent>) {
    if (
      _.isEmpty(this.store.snapshot().mockState.stockTradingDetails)
    ) {
      this.okClick()
    }
  }

  ngOnInit() {
    this.store.subscribe(({ mockState }) => {
      this.stockTradeDetails = _.get(mockState, "stockTradingDetails", null)
    })
  }

  okClick() {
    this.store.dispatch(new Navigate(["signals"]));
    this.dialogRef.close()
  }

  ngOnDestroy() {
    this.store.dispatch(
      new ResetSpecificMockStateAction({ stockTradingDetails: {} })
    );
  }

}
