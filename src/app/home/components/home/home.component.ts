import { Component, OnInit } from '@angular/core';
import { Store } from "@ngxs/store";
import * as _ from "lodash";
import { SaveTradingModeIconStatusAction } from '@app/home/ngxs-store/ngxs-actions/mock.actions';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  tradingMode: any
  constructor(private store: Store) { }

  ngOnInit() {
    this.store.dispatch(new SaveTradingModeIconStatusAction(true));

    this.store.subscribe(({ mockState }) => {
      this.tradingMode = _.get(mockState, "tradingMode", null)
    })

  }

}
