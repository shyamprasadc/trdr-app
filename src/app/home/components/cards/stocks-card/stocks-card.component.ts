import { Component, OnInit, Input } from '@angular/core';
import * as _ from "lodash";
import { Router } from "@angular/router";
import { mockData } from "@app/home/services/mock.service"
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";

@Component({
  selector: 'app-stocks-card',
  templateUrl: './stocks-card.component.html',
  styleUrls: ['./stocks-card.component.scss']
})
export class StocksCardComponent implements OnInit {
  @Input() filter = ""
  manualMode: any;
  walletbalance: number = 10000;
  allStocks: any;
  stocks: any;
  constructor(private router: Router, private store: Store) { }

  ngOnInit() {
    this.allStocks = _.get(mockData, "stocks")
    if (this.filter === "ALL") {
      this.stocks = this.allStocks
    } else {
      this.stocks = _.filter(this.allStocks, { status: this.filter })
    }
    this.store.subscribe(({ mockState }) => {
      const tradingMode = _.get(mockState, "tradingMode", null)
      if (tradingMode === "robo") {
        this.manualMode = false
      } else {
        this.manualMode = true
      }
    })
  }

  tradeRoute(data) {
    if (data.company) {
      console.log(data)
      this.store.dispatch(new Navigate(["trade-stocks", data.company]));
    }
  }

  statusColor(status) {
    if (status === "BUY") {
      return "green"
    } else if (status === "SELL") {
      return "red"
    } else {
      return "orange"
    }
  }

}
