import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TradingModeComponent } from '@app/home/components/trading-mode/trading-mode.component';
import { Store } from "@ngxs/store";
import * as _ from "lodash";

@Component({
  selector: 'app-trading-mode-card',
  templateUrl: './trading-mode-card.component.html',
  styleUrls: ['./trading-mode-card.component.scss']
})
export class TradingModeCardComponent implements OnInit {
  manualMode: any;

  constructor(private dialog: MatDialog, private store: Store) { }

  ngOnInit() {
    this.store.subscribe(({ mockState }) => {
      const tradingMode = _.get(mockState, "tradingMode", null)
      if (tradingMode === "robo") {
        this.manualMode = false
      } else {
        this.manualMode = true
      }
    })
  }
  toggleModeClick() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "80%";
    dialogConfig.maxWidth = "352px"

    this.dialog.open(TradingModeComponent, dialogConfig)
  }

}
