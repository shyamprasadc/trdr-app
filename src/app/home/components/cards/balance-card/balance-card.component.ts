import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-balance-card',
  templateUrl: './balance-card.component.html',
  styleUrls: ['./balance-card.component.scss']
})
export class BalanceCardComponent implements OnInit {
  walletbalance: number = 10000;

  constructor() { }

  ngOnInit() {
  }

}
