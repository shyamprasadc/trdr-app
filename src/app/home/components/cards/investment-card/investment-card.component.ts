import { Component, OnInit } from '@angular/core';
import { mockData } from "@app/home/services/mock.service";
import * as _ from "lodash";


@Component({
  selector: 'app-investment-card',
  templateUrl: './investment-card.component.html',
  styleUrls: ['./investment-card.component.scss']
})
export class InvestmentCardComponent implements OnInit {
  investments: any;
  constructor() { }

  ngOnInit() {
    this.investments = _.get(mockData, "investments")
  }

}
