import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from "@angular/material";
import { Store } from "@ngxs/store";
import * as _ from "lodash";
import {
  SaveTradingModeAction
} from "@app/home/ngxs-store/ngxs-actions/mock.actions";

@Component({
  selector: 'app-trading-mode',
  templateUrl: './trading-mode.component.html',
  styleUrls: ['./trading-mode.component.scss']
})
export class TradingModeComponent implements OnInit {
  tradingMode: any

  constructor(
    private store: Store,
    public dialogRef: MatDialogRef<TradingModeComponent>
  ) { }

  ngOnInit() {
    this.store.subscribe(({ mockState }) => {
      const tradingMode = _.get(mockState, "tradingMode", null);
      if (tradingMode === "robo") {
        this.tradingMode = "manual"
      } else {
        this.tradingMode = "robo"
      }
    })
  }

  toggleModeClick() {
    this.store.dispatch(new SaveTradingModeAction(this.tradingMode))
    this.dialogClose()
  }

  dialogClose() {
    this.dialogRef.close()
  }
}
