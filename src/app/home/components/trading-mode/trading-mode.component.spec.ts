import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingModeComponent } from './trading-mode.component';

describe('TradingModeComponent', () => {
  let component: TradingModeComponent;
  let fixture: ComponentFixture<TradingModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradingModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradingModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
