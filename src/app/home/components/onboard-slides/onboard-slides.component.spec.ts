import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardSlidesComponent } from './onboard-slides.component';

describe('OnboardSlidesComponent', () => {
  let component: OnboardSlidesComponent;
  let fixture: ComponentFixture<OnboardSlidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardSlidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardSlidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
