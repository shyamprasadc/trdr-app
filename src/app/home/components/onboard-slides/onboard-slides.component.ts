import { Component, OnInit } from '@angular/core';
import { OwlOptions, SlidesOutputData } from 'ngx-owl-carousel-o';
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";
import * as _ from "lodash";

@Component({
  selector: 'app-onboard-slides',
  templateUrl: './onboard-slides.component.html',
  styleUrls: ['./onboard-slides.component.scss']
})
export class OnboardSlidesComponent implements OnInit {
  activeSlides: SlidesOutputData;

  constructor(private store: Store) { }

  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: false
  }

  ngOnInit() {
  }

  getPassedData(data: SlidesOutputData) {
    this.activeSlides = data;
  }


  showButton() {
    const slidePostion = _.get(this.activeSlides, 'startPosition', 0);
    if (slidePostion == 2) {
      return true
    }
  }

  signInRoute() {
    this.store.dispatch(new Navigate(["sign-in"]));
  }

}
