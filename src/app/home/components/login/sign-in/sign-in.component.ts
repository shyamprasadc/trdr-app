import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";
import {
  SaveTradingModeAction,
} from "@app/home/ngxs-store/ngxs-actions/mock.actions";



@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  myModel = true;
  hide: boolean = true;
  inputForm: FormGroup;


  constructor(private router: Router, private store: Store) { }

  ngOnInit() {
    this.store.dispatch(new SaveTradingModeAction("robo"));
    this.inputForm = new FormGroup({
      userId: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required]),
    });
  }

  submit() {
    if (this.inputForm.valid) {
      this.store.dispatch(new Navigate(["home"]));
    }
  }

  signUpRoute() {
    this.store.dispatch(new Navigate(["sign-up"]));
  }

}
