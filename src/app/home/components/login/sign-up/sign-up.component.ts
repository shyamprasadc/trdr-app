import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  inputForm: FormGroup;
  hide: boolean = true;
  myModel = true;

  constructor(private router: Router, private store: Store) { }

  ngOnInit() {
    this.inputForm = new FormGroup({
      mobileNo: new FormControl("7012790741", [Validators.required,
      Validators.pattern(/^[0-9]\d*$/),
      Validators.minLength(10),
      Validators.maxLength(10)]),
      password: new FormControl("password", [Validators.required]),
      emailId: new FormControl("shuhaib@kred.in", [Validators.required, Validators.email]),
    });
  }

  submit() {
    if (this.inputForm.valid) {
      this.store.dispatch(new Navigate(["create-account"]));
    }

  }
  signInRoute() {
    this.store.dispatch(new Navigate(["sign-in"]));
  }


}
