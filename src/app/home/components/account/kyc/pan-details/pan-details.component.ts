import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";
@Component({
  selector: 'app-pan-details',
  templateUrl: './pan-details.component.html',
  styleUrls: ['./pan-details.component.scss']
})
export class PanDetailsComponent implements OnInit {
  inputForm: FormGroup;
  fileToUpload: File = null;

  constructor(private router: Router, private store: Store) { }

  ngOnInit() {
    this.inputForm = new FormGroup({
      panNumber: new FormControl(null, [Validators.required]),
      fatherName: new FormControl(null, [Validators.required]),
      firstName: new FormControl(null, [Validators.required]),
      lastName: new FormControl(null, [Validators.required]),
    });
  }
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  submit() {
    // if (this.inputForm.valid) {
    this.nextRoute()
    // }
  }

  nextRoute() {
    this.store.dispatch(new Navigate(["home"]));
  }

  backPath() {
    this.store.dispatch(new Navigate(["create-account"]));
  }

}
