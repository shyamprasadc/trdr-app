import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";

interface Occupation {
  value: string;
}
interface Marital {
  value: string;
}
interface Income {
  value: any;
}

@Component({
  selector: 'app-additional-details',
  templateUrl: './additional-details.component.html',
  styleUrls: ['./additional-details.component.scss']
})
export class AdditionalDetailsComponent implements OnInit {
  inputForm: FormGroup;
  occupationSelected = new FormControl('');
  maritalstatusSelected = new FormControl('');
  annualIncomeSelected = new FormControl('');

  occupation: Occupation[] = [
    { value: "PRIVATE SECTOR SERVIVE", },
    { value: "PUBLIC SECTOR", },
    { value: "BUSINESS", },
    { value: "PROFESSIONAL", },
    { value: "AGRICULTARIST", },
    { value: "RETIRED", },
    { value: "HOUSEWIFE", },
    { value: "STUDENT", },
    { value: "FOREX DEALER", },
    { value: "GOVERNMENT SERVICE", },
    { value: "OTHERS", },

  ];

  marital: Marital[] = [
    { value: "MARRIED", },
    { value: "SINGLE", },
    { value: "WIDOWED" }

  ];

  income: Income[] = [
    { value: "BELOW 1 LAC", },
    { value: "1-5 LAC", },
    { value: "5-10 LAC", },
    { value: "10-25 LAC", },
    { value: ">25 LAC", },

  ];

  constructor(private router: Router, private store: Store) { }

  ngOnInit() {
    this.inputForm = new FormGroup({
      occupationSelected: this.occupationSelected,
      maritalstatusSelected: this.maritalstatusSelected,
      annualIncomeSelected: this.annualIncomeSelected,
      networthValue: new FormControl(null, [Validators.required]),
    });
  }

  submit() {
    // if (this.inputForm.valid) {
    this.nextRoute()
    // }
  }

  nextRoute() {
    this.store.dispatch(new Navigate(["home"]));
  }

  backPath() {
    this.store.dispatch(new Navigate(["bank-details"]));
  }
}
