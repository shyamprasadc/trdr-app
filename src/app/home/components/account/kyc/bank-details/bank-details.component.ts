import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";


@Component({
  selector: 'app-bank-details',
  templateUrl: './bank-details.component.html',
  styleUrls: ['./bank-details.component.scss']
})
export class BankDetailsComponent implements OnInit {
  inputForm: FormGroup;
  fileToUpload: File = null;

  constructor(private router: Router, private store: Store) { }

  ngOnInit() {
    this.inputForm = new FormGroup({
      bankAccountNumber: new FormControl(null, [Validators.required]),
      bankAccountName: new FormControl(null, [Validators.required]),
      bankName: new FormControl(null, [Validators.required]),
      ifsc:new FormControl(null, [Validators.required]),
      branchName:new FormControl(null, [Validators.required]),
    });
  }
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  submit() {
    // if (this.inputForm.valid) {
    this.nextRoute()
    // }
  }

  nextRoute() {
    this.store.dispatch(new Navigate(["aadhaar-details"]));
  }

  backPath() {
    this.store.dispatch(new Navigate(["aadhaar-details"]));
  }

}
