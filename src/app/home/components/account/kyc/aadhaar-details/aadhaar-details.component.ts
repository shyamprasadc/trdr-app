import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";
interface City {
  value: string;
}
interface State {
  value: string;
}

@Component({
  selector: 'app-aadhaar-details',
  templateUrl: './aadhaar-details.component.html',
  styleUrls: ['./aadhaar-details.component.scss']
})
export class AadhaarDetailsComponent implements OnInit {
  inputForm: FormGroup;
  fileToUpload: File = null;
  citySelected = new FormControl('');
  stateSelected = new FormControl('');

  city: City[] = [
    { value: "Ernakulam", },
    { value: "Calicut", },
    { value: "Banglore", },

  ];

  state: State[] = [
    { value: "Kerala", },
    { value: "Karanataka", },

  ];

  constructor(private router: Router, private store: Store) { }

  ngOnInit() {
    this.inputForm = new FormGroup({
      aadhaarNumber: new FormControl(null, [Validators.required]),
      city: new FormControl(null, [Validators.required]),
      state: new FormControl(null, [Validators.required]),
      addressAadhaar: new FormControl(null, [Validators.required]),
    });
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  submit() {
    // if (this.inputForm.valid) {
    this.nextRoute()
    // }
  }

  nextRoute() {
    this.store.dispatch(new Navigate(["pan-details"]));
  }

  backPath() {
    this.store.dispatch(new Navigate(["bank-details"]));
  }

}
