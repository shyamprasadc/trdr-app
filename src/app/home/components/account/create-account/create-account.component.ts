import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";
import { MatDialog, MatDialogConfig } from '@angular/material';
import { IntroAccountComponent } from '@app/home/components/account/intro-account/intro-account.component';

interface Status {
  value: string;
}


@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})

export class CreateAccountComponent implements OnInit {
  inputForm: FormGroup;
  loanStatusForm = new FormControl('LOAN1', [Validators.required]);
  partnerForm = new FormControl('PARTNER1', [Validators.required]);
  loanStatus: Status[] = [
    {
      value: "LOAN1",
    },
    {
      value: "LOAN2",
    },
    {
      value: "LOAN3",
    },
    {
      value: "LOAN4",
    },
    {
      value: "LOAN5",
    },
  ];
  partnerStatus: Status[] = [
    {
      value: "PARTNER"
    },
    {
      value: "PARTNER2"
    }
  ]

  constructor(private router: Router, private store: Store, private dialog: MatDialog) { }

  ngOnInit() {
    this.inputForm = new FormGroup({
      partner: this.partnerForm,
      operatingDate: new FormControl(new Date(), [Validators.required]),
      loanStatus: this.loanStatusForm,
      companyName: new FormControl("Anand singhi", [Validators.required]),
      operating: new FormControl("Sole proprietorship", [Validators.required]),
      city: new FormControl("Bangalore", [Validators.required]),
      pin: new FormControl("288888", [Validators.required]),
    });
  }
  submit() {
    if (this.inputForm.valid) {
      this.popUpConfirmation()
    }
  }

  backPath() {
    this.store.dispatch(new Navigate(["sign-up"]));
  }

  popUpConfirmation() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "80%";
    dialogConfig.maxWidth = "352px"

    this.dialog.open(IntroAccountComponent, dialogConfig)
  }

}
