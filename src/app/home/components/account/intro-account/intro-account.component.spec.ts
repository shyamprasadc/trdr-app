import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroAccountComponent } from './intro-account.component';

describe('IntroAccountComponent', () => {
  let component: IntroAccountComponent;
  let fixture: ComponentFixture<IntroAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
