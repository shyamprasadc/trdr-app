import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";
import { MatDialogRef } from "@angular/material";


@Component({
  selector: 'app-intro-account',
  templateUrl: './intro-account.component.html',
  styleUrls: ['./intro-account.component.scss']
})
export class IntroAccountComponent implements OnInit {
  myModel = true;

  constructor(private router: Router, private store: Store, public dialogRef: MatDialogRef<IntroAccountComponent>) { }

  ngOnInit() {
  }

  okClick() {
    this.store.dispatch(new Navigate(["bank-details"]));
    this.dialogRef.close()
  }
}
