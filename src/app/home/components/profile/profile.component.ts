import { Component, OnInit } from '@angular/core';
import { Store } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";
import { SaveTradingModeIconStatusAction } from '@app/home/ngxs-store/ngxs-actions/mock.actions';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  walletbalance: number = 10000;

  constructor(private store: Store) { }

  ngOnInit() {
  }

  signOut() {
    this.store.dispatch(new SaveTradingModeIconStatusAction(false));
    this.store.dispatch(new Navigate(["sign-in"]));
  }

}
