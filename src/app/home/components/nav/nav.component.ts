import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  Renderer2,
} from "@angular/core";
import { NavigationEnd, Router, ActivatedRoute } from "@angular/router";

import { MatSidenav } from "@angular/material";

import { Subscription } from "rxjs";

import { TranslateService } from "@ngx-translate/core";

import { AuthService } from "auth";

import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";

import { ConfigService, LoggerService } from "utils";
import * as _ from "lodash";
import { Store } from "@ngxs/store";
import { HideToastAction } from "@app/home/ngxs-store/ngxs-actions/toast.actions";
import { StateResetAll } from "ngxs-reset-plugin";

// interface SideNavRoute {
//   icon?: string;
//   route?: string;
//   title?: string;
// }

export interface Menu {
  cols: number;
  rows: number;
  text: string;
  route: string;
  icon: string;

  isSelected: boolean;
  isDisabled: string;
  path: string[];
}
export interface HamburgerMenu {
  text: string;
  route: string;
  icon: string;
  // isSelected: boolean;
  // isDisabled: string;
}

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.scss"],
})
export class NavComponent implements OnInit, OnDestroy {
  hamburgerMenu: HamburgerMenu[] = [
    {
      text: "My Profile",
      route: "profile",
      icon: "account_box",
    },
  ];

  menuLists: Menu[] = [
    {
      text: "Home",
      cols: 1,
      rows: 1,
      route: "home",
      icon: "home",
      isSelected: true,
      isDisabled: "unset",
      path: ["/home"],
    },
    {
      text: "Customer",
      cols: 1,
      rows: 1,
      route: "portfolio",
      icon: "work",
      isSelected: false,
      isDisabled: "unset",
      path: ["/portfolio"],
    },
    {
      text: "test2",
      cols: 1,
      rows: 1,
      route: "signals",
      icon: "timeline",
      isSelected: false,
      isDisabled: "unset",
      path: ["/signals"],
    },
    {
      text: "test3",
      cols: 1,
      rows: 1,
      route: "review",
      icon: "assignment",
      isSelected: false,
      isDisabled: "unset",
      path: ["/review"],
    },
    {
      text: "Profile",
      cols: 1,
      rows: 1,
      route: "profile",
      icon: "account_box",
      isSelected: false,
      isDisabled: "unset",
      path: ["/profile"],
    },
  ];
  private returnUrl = "/signin";
  public showFooterMenu: boolean = true;

  @ViewChild("commandbarSidenav", { static: true })
  public sidenav: MatSidenav;

  protected subscription: Subscription;
  public userInfo: any = "";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private configService: ConfigService,
    private translate: TranslateService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private logger: LoggerService,
    private store: Store
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.returnUrl = event.url;
        this.menuLists = this.menuLists.map((data) => {
          if (data.path.includes(this.returnUrl)) {
            return { ...data, isSelected: true };
          } else return { ...data, isSelected: false };
        });

        this.showFooterMenu =
          route.root.firstChild.snapshot.data["showFooterMenu"] === undefined
            ? true
            : route.root.firstChild.snapshot.data["showFooterMenu"];
      }
    });
  }

  public ngOnInit(): void {
    this.logger.info("NavComponent: ngOnInit()");

    this.store.subscribe((store) => {
      if (store) {
        const { loaderState, toastState, userState } = store;
        if (loaderState.loading) {
          this.spinner.show();
          this.spinner.show();
        } else {
          this.spinner.hide();
        }
        if (toastState) {
          if (toastState.toast) {
            this.showToast(toastState.toastType, toastState.toastMessage);
            this.store.dispatch(new HideToastAction());
          }
        }
      }
    });

    // this.subscribe();
  }

  public getUserInfo(): any {
    if (this.authService.getUser()) this.userInfo = this.authService.getUser();

    return this.userInfo;
  }

  public showToast(type: string, message: string, title: string = null): void {
    const header: string = _.isEmpty(title) ? _.capitalize(type) : title;
    switch (type) {
      case "error":
        this.toastr.error(message, header);
        break;
      case "info":
        this.toastr.info(message, header);
        break;
      case "success":
        this.toastr.success(message, header);
        break;
      case "warning":
        this.toastr.warning(message, header);
        break;
    }
  }

  protected unsubscribe() {
    this.logger.info("DashboardComponent: unsubscribe()");

    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  public isAuthenticated() {
    return this.authService.isAuthenticated();
  }

  public onDragStart(event, identifier) {
    this.logger.info("NavComponent: onDragStart()");

    event.dataTransfer.setData("widgetIdentifier", identifier);

    event.dataTransfer.setData("text/plain", "Drag Me Button");
    event.dataTransfer.dropEffect = "move";
  }

  public logout() {
    // localStorage.setItem("userExtraDetails", null);
    // localStorage.setItem("userData", null);
    // localStorage.setItem("token", null);
    // this.store.dispatch(new StateResetAll());

    // this.authService.logout("/signin");
  }

  public ngOnDestroy() {
    this.logger.info("NavComponent: ngOnDestroy()");

    this.unsubscribe();
  }
}

// https://github.com/tiberiuzuld/angular-gridster2/blob/master/src/app/sections/emptyCell/emptyCell.component.html
// https://github.com/tiberiuzuld/angular-gridster2/blob/master/src/app/sections/emptyCell/emptyCell.component.ts

// this.logger.info('toolPaletteItems: ' + JSON.stringify(this.toolPaletteItems));
