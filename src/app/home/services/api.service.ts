import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { ModelFactory, MODEL_PATHS, SessionChallenge } from "pk-client";
import { AuthService } from "auth";
import { environment } from "@env/environment";
import * as _ from "lodash";
import { ShowLoaderAction } from "../ngxs-store/ngxs-actions/loader.actions";
import { Store } from "@ngxs/store";
import { CustomHttpParamEncoder } from "./CustomHttpParamEncoder";

const BASE_URL = environment.apiBaseURL;

@Injectable({
  providedIn: "root",
})
export class ApiService {
  token: string = _.isEmpty(this.authService.getAccessToken())
    ? ""
    : this.authService.getAccessToken();
  userInfo: any = JSON.parse(localStorage.getItem("userData"));
  constructor(
    private httpClient: HttpClient,
    private authService: AuthService,
    private store: Store
  ) { }

  public login(params: {
    email: string;
    password: string;
  }): Observable<Object> {
    this.showLoader();
    const url = `${BASE_URL}/main/auth/sign-in`;
    // const body = new HttpParams()
    //   .set("email", params.email)
    //   .set("password", params.password);
    const body = this.stringParams(params).toString();
    return this.httpClient
      .post<Object>(url, body, {
        headers: new HttpHeaders().set(
          "Content-Type",
          "application/x-www-form-urlencoded"
        ),
      })
      .pipe(
        // retry(2),
        catchError(this.handleError)
      );
  }

  stringParams(params: object = {}) {
    let body = new HttpParams({ encoder: new CustomHttpParamEncoder() });
    _.forEach(params, (value, key) => {
      body = body.set(key, value);
    });
    return body;
  }

  public post(
    url: string,
    params: object = {},
    isUrlEncoded: boolean = true
  ): Observable<Object> {
    this.showLoader();
    const postUrl = `${BASE_URL}/${url}`;

    let headers = new HttpHeaders();
    const token: string = _.isEmpty(this.authService.getAccessToken())
      ? ""
      : this.authService.getAccessToken();
    const tokenType = _.get(this.userInfo, "token_type", "Bearer");
    headers = headers.set("Authorization", `${tokenType} ${token}`);

    if (isUrlEncoded) {
      const body = this.stringParams(params).toString();
      headers = headers.set(
        "Content-Type",
        "application/x-www-form-urlencoded"
      );
      return this.httpClient
        .post<Object>(postUrl, body, {
          headers,
        })
        .pipe(
          // retry(2),
          catchError(this.handleError)
        );
    } else {
      return this.httpClient
        .post<Object>(postUrl, params, { headers })
        .pipe(
          // retry(2),
          catchError(this.handleError)
        );
    }
  }

  //   public get(
  //     url: string,
  //     params: object = { test: 1234, test1: "abc" }
  //   ): Observable<Object> {
  //     const getUrl = `${BASE_URL}/${url}`;
  //     console.log(
  //       "=============this.stringParams(params)======================="
  //     );
  //     console.log(this.stringParams(params));
  //     console.log("====================================");
  //     return this.httpClient
  //       .get<Object>(getUrl, {
  //         headers: new HttpHeaders().set("Content-Type", "application/json"),
  //         params: this.stringParams(params)
  //       })
  //       .pipe(retry(2), catchError(this.handleError));
  //   }

  public get(
    url: string,
    params: object = {},
    isUrlEncoded: boolean = true
  ): Observable<Object> {
    this.showLoader();
    const getUrl = `${BASE_URL}/${url}`;

    const headerType: string = isUrlEncoded
      ? "application/x-www-form-urlencoded"
      : "application/json";
    let headers = new HttpHeaders();
    const isTokenAvailable = _.isEmpty(this.authService.getAccessToken());
    const token: string = isTokenAvailable
      ? ""
      : this.authService.getAccessToken();
    const tokenType = _.get(this.userInfo, "token_type", "Bearer");

    headers = headers.set("Content-Type", headerType);
    if (!_.isEmpty(token)) {
      headers = headers.set("Authorization", `${tokenType} ${token}`);
    }
    return this.httpClient
      .get<Object>(getUrl, {
        params: { ...params },
        headers,
      })
      .pipe(
        // retry(2),
        catchError(this.handleError)
      );
  }

  public put(
    url: string,
    params: object = {},
    isUrlEncoded: boolean = true
  ): Observable<Object> {
    this.showLoader();
    const postUrl = `${BASE_URL}/${url}`;
    let headers = new HttpHeaders();
    const token: string = _.isEmpty(this.authService.getAccessToken())
      ? ""
      : this.authService.getAccessToken();
    const tokenType = _.get(this.userInfo, "token_type", "Bearer");

    headers = headers.set("Authorization", `${tokenType} ${token}`);

    if (isUrlEncoded) {
      const body = this.stringParams(params).toString();

      headers = headers.set(
        "Content-Type",
        "application/x-www-form-urlencoded"
      );

      return this.httpClient
        .put<Object>(postUrl, body, {
          headers,
        })
        .pipe(
          // retry(2),
          catchError(this.handleError)
        );
    } else {
      return this.httpClient
        .put<Object>(postUrl, params, { headers })
        .pipe(
          // retry(2),
          catchError(this.handleError)
        );
    }
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  showLoader() {
    this.store.dispatch(new ShowLoaderAction());
  }

  // GraphQl

  public async query(
    path: string,
    url: string,
    params: object,
    returnData: string,
    resolveMockData: boolean = false,
    mockPath: string = ""
  ): Promise<Object> {
    this.showLoader();
    // const payismEndpointl = `http://13.233.208.231:9443/graphql/payism-dmr/${url}`;
    const bankitEndpoint = `http://13.233.208.231:9443/graphql/bankit-dmr/${url}`;
    const baseUrl = "http://13.233.208.231:9443/graphql";
    const mf = new ModelFactory({
      // api_base_url: `${environment.apiBaseURL}${environment.gqlEndPoint}`,
      // payism url
      api_base_url: baseUrl,
      // token: this.authService.getAccessToken(),
      token:
        "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZXNzaW9uIjoiNWVlOGFjMWI5ZGQyNjUxZGU2YTdhZDBjIiwiaWF0IjoxNTkyMzA2NzE2LCJleHAiOjE2MjM4NDI3MTZ9.G8a8AKHamDsRKSdnJI8Xo5Qw5_7laezWPTy9nj7hahbOjTRCK6ip8digE6IQdsi-QaHJIkyix6qdijyjvjwxzM6v40EOHGhsiMCmTQO9TWHLimQo5VgeOe2fldwFJqF5XXtwUi0TTHFaIq1sHx8lkGtoGoYFJv4JeGO0Ti-hufY",
    });
    if (!resolveMockData) {
      return await mf.gqlclient(path).query(url, params, returnData);
    }
  }

  public async mutation(
    path: string,
    url: string,
    params: object,
    returnData: string,
    resolveMockData: boolean = false,
    mockPath: string = ""
  ): Promise<Object> {
    this.showLoader();

    // const payismEndpointl = `http://13.233.208.231:9443/graphql/payism-dmr`;
    const bankitEndpoint = `http://13.233.208.231:9443/graphql/bankit-dmr`;
    const baseUrl = "http://13.233.208.231:9443/graphql";

    const mf = new ModelFactory({
      // api_base_url: `${environment.apiBaseURL}${environment.gqlEndPoint}`,
      api_base_url: `${baseUrl}`,
      // token: this.authService.getAccessToken(),
      token:
        "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZXNzaW9uIjoiNWVlOGFjMWI5ZGQyNjUxZGU2YTdhZDBjIiwiaWF0IjoxNTkyMzA2NzE2LCJleHAiOjE2MjM4NDI3MTZ9.G8a8AKHamDsRKSdnJI8Xo5Qw5_7laezWPTy9nj7hahbOjTRCK6ip8digE6IQdsi-QaHJIkyix6qdijyjvjwxzM6v40EOHGhsiMCmTQO9TWHLimQo5VgeOe2fldwFJqF5XXtwUi0TTHFaIq1sHx8lkGtoGoYFJv4JeGO0Ti-hufY",
    });
    if (!resolveMockData) {
      return await mf.gqlclient(path).mutation(url, params, returnData);
    }
  }

  // public get(url: string, params: object): Observable<Object> {
  //   return this.httpClient.get<Object>(url, { params: { ...params } }).pipe(
  //     retry(2),
  //     catchError(this.handleError)
  //   );
  // }

  // handleError(error: HttpErrorResponse) {
  //   return throwError(error);
  // }

  // public post(url: string, params: object) {
  //   this.httpClient
  //     .post<any>(url, { params: { ...params } })
  //     .subscribe(data => {
  //       console.log("==============Response data======================");
  //       console.log(data);
  //       console.log("====================================");
  //     });
  // }

  // public put(url: string, params: object) {
  //   this.httpClient.put<any>(url, { params: { ...params } }).subscribe(data => {
  //     console.log("==============Response data======================");
  //     console.log(data);
  //     console.log("====================================");
  //   });
  // }

  // public delete(url: string, params: object) {
  //   this.httpClient
  //     .delete<any>(url, { params: { ...params } })
  //     .subscribe(data => {
  //       console.log("==============Response data======================");
  //       console.log(data);
  //       console.log("====================================");
  //     });
  // }
}
