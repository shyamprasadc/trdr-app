//Payism API query

export const checkRemitterQuery: string =
  "Status FirstName LastName RemitterNo RemitterLimit Description errors{ status}";

export const getRemitterBeneficiaryListsQuery: string =
  "Status Description RemitterNo BeneficiaryList{ beneficiaryId uuid accountDetail{ accountNumber ifscCode bankName accountHolderName } } errors{ status}";

export const addRemitterBeneficiaryQuery: string =
  "Status RemitterNo BeneficiaryID Description errors{ status }";

export const registerRemitterQuery: string =
  "Status OTPCode Description errors{ status }";

export const verifyRemitterQuery: string =
  "Status Description errors{ status }";

export const fundTransferQuery: string =
  "Status EcapsTransID APIPartnerTransID BeneficiaryID Amount FeeRecovery ServiceFee ServiceFeeGST TotalServiceFee CommissionAmount CommissionAmountWithoutGST GSTOnCommission TDSRateOnCommission TDSAmountOnCommission NetCommissionAfterTDS Channel AmountDeducted RRN BeneficiaryName Description errors{ status }";

export const transactionStatus: string =
  "Status EcapsTransID PartnerTransID RemitterNumber TransactionTime BeneficiaryID Amount Channel RRN RefundAmount Description errors{ status }  ";

// Bankit API query

export const fetchCustomerQuery = `data{
  customerId
  dateOfBirth
  kycstatus
  name
  walletbal
}    
errorCode
errorMsg
errors{
  status
  errorCode
  errorMsg
}`;

export const createCustomerQuery = `data{
  customerId
}
errorCode
errorMsg
errors{
  status
  errorCode
  errorMsg
}`;

export const customerRegistrationOtpQuery = `data{
  customerId
}
errorCode
errorMsg
errors{
  status
  errorCode
  errorMsg
}`;

export const fetchAllBeneficiariesQuery = `data{
  recipientList{
    recipientId
    recipientName
    mobileNo
    bankName
    channel
    udf1
    udf2
  }
}
errorCode
errorMsg
errors{
  status
  errorCode
  errorMsg
}`;

export const deleteBeneficiaryQuery = `errorCode
errorMsg
errors{
  status
  errorCode
  errorMsg
}`;

export const verifyBeneficiaryQuery = `data{
  customerId
  name
  clientRefId
  txnId      
}
errorCode
errorMsg
errors{
  status
  errorCode
  errorMsg
}`;

export const impsMoneyTransferQuery = `data{
  customerId
  name
  clientRefId
  txnId
}
errorMsg
errorCode
errors{
  status
  errorCode
  errorMsg
}`;

export const neftMoneyTransferQuery = `data{
  customerId
  clientRefId
  txnId
}
errorMsg
errorCode
errors{
  status
  errorCode
  errorMsg
}`;

export const addCustomerBeneficiaryQuery = `data{
  customerId
  recipientId
  name
  kycstatus
  walletbal
  dateOfBirth
  recipientName
  mobileNo
}
errorCode
errorMsg
errors{
  status
  errorCode
  errorMsg
}`;

export const fetchBankListsQuery = `data{
  bankList{
    bankCode
    bankName
    channelsSupported
    accVerAvailabe
    ifsc
    ifscStatus
  }
}
errors{
  status
  errorCode
  errorMsg
}`;

export const getPendingTransctionListQuery = `data{
  transactionDetails{
    customerId
    clientRefId
    transactionDate
    amount
  }
}
errorMsg
errorCode
errors{
  status
  errorCode
  errorMsg
}`;

export const getTransactionStatusQuery = `data{
  clientRefId
  transactionDate
  amount
}
errorMsg
errorCode
errors{
  status
  errorCode
  errorMsg
}`;
