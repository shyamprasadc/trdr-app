export const mockData = {
    stocks: [
        {
            company: "AXISBANK",
            status: "BUY",
            price: 448.05,
            target: 454,
            stopLoss: 445,
            change: 3.93
        },
        {
            company: "BRITANNIA",
            status: "HOLD",
            price: 3883.00,
            target: 0,
            stopLoss: 0,
            change: -1.52
        },
        {
            company: "BAJAJAUTO",
            status: "BUY",
            price: 3004.00,
            target: 3058,
            stopLoss: 2997,
            change: 0.34
        },
        {
            company: "INFY",
            status: "SELL",
            price: 950.50,
            target: 944,
            stopLoss: 963,
            change: -0.09
        },
        {
            company: "ONGC",
            status: "BUY",
            price: 78.50,
            target: 81,
            stopLoss: 77,
            change: -0.70
        },
        {
            company: "WIPRO",
            status: "BUY",
            price: 279.95,
            target: 285,
            stopLoss: 274,
            change: 0.92
        },
        {
            company: "ULTRACEMCO",
            status: "HOLD",
            price: 3972.35,
            target: 0,
            stopLoss: 0,
            change: .10
        },
        {
            company: "HINDUNILVR",
            status: "BUY",
            price: 2212.75,
            target: 2251,
            stopLoss: 2206,
            change: 0.27
        },
        {
            company: "HCLTECH",
            status: "BUY",
            price: 684.25,
            target: 690,
            stopLoss: 677,
            change: -1.13
        },
        {
            company: "HEROMOTOCO",
            status: "SELL",
            price: 2719.95,
            target: 2679,
            stopLoss: 2733,
            change: 0.66
        },
        {
            company: "LT",
            status: "BUY",
            price: 961.50,
            target: 969,
            stopLoss: 950,
            change: 0.60
        },
        {
            company: "RELIANCE",
            status: "BUY",
            price: 2130.50,
            target: 2156,
            stopLoss: 2114,
            change: 0.50
        },
        {
            company: "TITAN",
            status: "SELL",
            price: 1068.90,
            target: 1053,
            stopLoss: 1074,
            change: -3.49
        },
        {
            company: "MARUTI",
            status: "BUY",
            price: 6648.75,
            target: 6759,
            stopLoss: 6625,
            change: 0.60
        },
        {
            company: "KOTAKBANK",
            status: "BUY",
            price: 1363.10,
            target: 1379,
            stopLoss: 1352,
            change: 0.21
        },
        {
            company: "YESBANK",
            status: "HOLD",
            price: 15.50,
            target: 0,
            stopLoss: 0,
            change: 4.73
        },
        {
            company: "ITC",
            status: "BUY",
            price: 203.35,
            target: 209,
            stopLoss: 201,
            change: 2.26
        },
        {
            company: "HDFCBANK",
            status: "BUY",
            price: 1070.95,
            target: 1080,
            stopLoss: 1058,
            change: 1.93
        },

    ],
    investments: [
        {
            company: "ONGC",
            amount: 1173,
            pl: 0,
            buyAvg: 87,
            sellAvg: 0,
            holdingQty: 15,
            buyQty: 15,
            soldQty: 0
        },
        {
            company: "HDFCBANK",
            amount: 4251.80,
            pl: 0,
            buyAvg: 1021.10,
            sellAvg: 0,
            holdingQty: 4,
            buyQty: 4,
            soldQty: 0
        },
        {
            company: "WIPRO",
            amount: 24835.50,
            pl: 0,
            buyAvg: 353.35,
            sellAvg: 3,
            holdingQty: 90,
            buyQty: 90,
            soldQty: 0
        },
        {
            company: "ICICIBANK",
            amount: 3655.00,
            pl: 0,
            buyAvg: 353.35,
            sellAvg: 6,
            holdingQty: 10,
            buyQty: 10,
            soldQty: 0
        },
        {
            company: "ITC",
            amount: 2042.50,
            pl: 0,
            buyAvg: 199.65,
            sellAvg: 199.65,
            holdingQty: 10,
            buyQty: 50,
            soldQty: 40
        },
        {
            company: "INFY",
            amount: 99070.40,
            pl: 0,
            buyAvg: 704.77,
            sellAvg: 0,
            holdingQty: 104,
            buyQty: 104,
            soldQty: 0
        },
    ]
}