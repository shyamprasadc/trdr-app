import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

//
// Auth libs
//

import { AuthGuard } from "auth";

import { OnboardSlidesComponent } from './home/components/onboard-slides/onboard-slides.component';
import { SignInComponent } from './home/components/login/sign-in/sign-in.component';
import { SignUpComponent } from './home/components/login/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './home/components/login/forgot-password/forgot-password.component';
import { ReviewComponent } from './home/components/review/review.component';
import { ProfileComponent } from './home/components/profile/profile.component';
import { VerifyNumberComponent } from './home/components/login/verify-number/verify-number.component';
import { CreateAccountComponent } from './home/components/account/create-account/create-account.component';
import { PanDetailsComponent } from './home/components/account/kyc/pan-details/pan-details.component';
import { AadhaarDetailsComponent } from './home/components/account/kyc/aadhaar-details/aadhaar-details.component';
import { BankDetailsComponent } from './home/components/account/kyc/bank-details/bank-details.component';
import { AdditionalDetailsComponent } from './home/components/account/kyc/additional-details/additional-details.component';
import { HomeComponent } from './home/components/home/home.component';
import { PortfolioComponent } from './home/components/portfolio/portfolio.component';
import { SignalsComponent } from './home/components/signals/signals.component';
import { TradeStocksComponent } from './home/components/stocks/trade-stocks/trade-stocks.component';


const routes: Routes = [
  {
    path: "home",
    component: HomeComponent,
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "portfolio",
    component: PortfolioComponent,
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "signals",
    component: SignalsComponent,
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "sign-in",
    component: SignInComponent,
    data: {
      // showHeaderNavBar: false,
      showFooterMenu: false,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "onboard",
    component: OnboardSlidesComponent,
    data: {
      showHeaderNavBar: false,
      showFooterMenu: false,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "sign-up",
    component: SignUpComponent,
    data: {
      // showHeaderNavBar: false,
      showFooterMenu: false,
      showBackButton: true,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "forgot-password",
    component: ForgotPasswordComponent,
    data: {
      // showHeaderNavBar: false,
      showFooterMenu: false,
      showBackButton: true,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "review",
    component: ReviewComponent,
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "trade-stocks/:company",
    component: TradeStocksComponent,
    data: {
      // showHeaderNavBar: false,
      showBackButton: true,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "profile",
    component: ProfileComponent,
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "create-account",
    component: CreateAccountComponent,
    data: {
      // showHeaderNavBar: false,
      showFooterMenu: false,
      showBackButton: true,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "verify-number",
    component: VerifyNumberComponent,
    data: {
      // showHeaderNavBar: false,
      showFooterMenu: false,
      showBackButton: true,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "pan-details",
    component: PanDetailsComponent,
    data: {
      // showHeaderNavBar: false,
      showFooterMenu: false,
      showBackButton: true,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "aadhaar-details",
    component: AadhaarDetailsComponent,
    data: {
      // showHeaderNavBar: false,
      showFooterMenu: false,
      showBackButton: true,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "bank-details",
    component: BankDetailsComponent,
    data: {
      // showHeaderNavBar: false,
      showFooterMenu: false,
      showBackButton: true,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "additional-details",
    component: AdditionalDetailsComponent,
    data: {
      // showHeaderNavBar: false,
      showFooterMenu: false,
      showBackButton: true,
    },
    // canActivate: [AuthGuard],
    // runGuardsAndResolvers: "always",
  },
  {
    path: "**",
    redirectTo: "sign-in",
  },
];

// const AuthActivatedRoute: Routes = routes.map((route) => {
//   if (["signin", "forgot-password"].includes(route.path)) {
//     return { ...route };
//   }

//   return {
//     canActivate: [AuthGuard],
//     runGuardsAndResolvers: "always",
//     ...route,
//   };
// });
@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: "reload" })],
  exports: [RouterModule],
})
export class AppRoutingModule { }