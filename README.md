# trdr: simple stock investing app

## Steps to Run

### Install Angular CLI

```
npm install -g @angular/cli
```

## Run Angular Project

```
npm install
npm run-script library
ng serve --port 8080
```

## Building For Production

### Step 1 Configure Webpack

```
change "node: false" key to "node: {crypto: true}" in browser.js file on node_modules folder

node_modules\@angular-devkit\build-angular\src\angular-cli-files\models\webpack-configs\browser.js
```

### Step 2 Run build Process

```
npm run-script library
ng build --prod

The output folder is www by default.
To output to a different folder, change the outputPath in angular.json.
```

## Capacitor Wrapper

```
Capacitor Required Dependencies

The base requirements are Node v8.6.0 or later, and NPM version 5.6.0 or later.

iOS Development
For building iOS apps, Capacitor requires a Mac with Xcode 11 or above. Or you can use Ionic Appflow to build for iOS even if you're on Windows.

Additionally, you'll need to install CocoaPods (sudo gem install cocoapods), and install the Xcode Command Line tools
(either from Xcode, or running xcode-select --install).

Android Development
Android development requires the Android SDK installed with Android Studio. Technically, Android Studio isn't required as you can
build and run apps using only the Android CLI tools.

Capacitor is targeting API level 21 or greater, meaning Android 5.0 (Lollipop) or above.

Also, Capacitor requires an Android WebView with Chrome version 50 or later. On Android 5 and 6, the Capacitor uses the System WebView.
On Android 7+, Google Chrome is used.
```

### Step 1 Production Build to WWW folder

```
"outputPath": "www" in angular.json file
ng build --prod
```

### Step 2 Install and Initialize Capacitor

```
npm install @capacitor/core @capacitor/cli
npx cap init
```

### Step 3 Creating Android Project

```
npx cap add android
npx cap sync
npx cap open android (Project opens on Android Studio)
```

### Step 4 Creating iOS Project

```
npx cap add ios
npx cap sync
npx cap open ios (Project opens on Xcode)
```

### Error Fixes

```
net::ERR_CLEARTEXT_NOT_PERMITTED on running android apk
Add <application android:usesCleartextTraffic="true"> in AndroidManifest.xml file
```
