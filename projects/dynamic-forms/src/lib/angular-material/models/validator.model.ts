export interface ValidatorModel {

  name: string;
  args: any;
  propertyName: string;
  message: string;

}

// https://github.com/udos86/ng-dynamic-forms/blob/master/packages/home/src/model/misc/dynamic-form-control-validation.model.ts
